import 'package:flutter/material.dart';
import 'package:flutter_firebase_auth/user_dashboard.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
        primaryColor: const Color(0xFFe74c3c),
        primaryColorDark: const Color(0xFFc0392b),
        accentColor: const Color(0xFFc0392b),
      ),
      debugShowCheckedModeBanner: false,
      title: 'Still Luv',
      home: new UserDashboard(),
    );
  }
}
